/*
 * This file is part of the Defenestrate project.
 *
 * Copyright 2010-2014 Eric Betts <bettse@gmail.com>
 *
 * Defenestrate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Defenestrate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Defenestrate.  If not, see <http://www.gnu.org/licenses/>.
 */

#import "Defenestrate.h"

#import "JRSwizzle/JRSwizzle.h"
#import <Carbon/Carbon.h>


@interface NSObject (Defenestrate)
- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication;
@end

@implementation NSObject (Defenestrate)
- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication
{
    return YES;
}
@end


@implementation Defenestrate

static Defenestrate *sharedInstance;

+ (void) load
{
#ifdef DEBUG
	NSLog(@"Defenestrate loaded.");
#endif

  [NSScreen jr_swizzleMethod:@selector(applicationShouldTerminateAfterLastWindowClosed:) withMethod:@selector(myApplicationShouldTerminateAfterLastWindowClosed:) error:NULL];
}

+ (void)initialize {
	static BOOL initialized = NO;
	if (!initialized) {
		sharedInstance = [[Defenestrate alloc] init];
		initialized = YES;
	}
}

+ (Defenestrate *)sharedInstance {
	return sharedInstance;
}

- (id) init {
	if (sharedInstance) {
		[self dealloc];
		return sharedInstance;
	}
	return self;
}

@end
